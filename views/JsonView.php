<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 19/06/2017
 * Time: 09:01
 */
class JsonView
{
    /**
     * @param $content
     * @return bool
     */
    public function render($content)
    {
        if(isset($content)){
            echo json_encode($content);
            return true;
        }else{
            echo json_encode('Staff Id Not Found!');
            return false;
        }
    }
}