<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 22/06/2017
 * Time: 13:31
 */
class StaffObject
{
    private $__id;
    private $__name;
    private $__age;
    private $__phone;
    private $__is_search = false;
    private $__search_condition = '';

    /**
     * StaffObject constructor.
     * @param $request
     */
    function __construct ($request)
    {
        if (!empty($request->url_elements[1])) {
            if ($request->url_elements[1] == 'search') {
                $this->__is_search = true;
                $this->set_search_condition($request);
                return false;
            }
            if (!is_numeric($request->url_elements[1]))
                return false;
            $this->__id = $request->url_elements[1];
        }
        if (isset($request->parameters['name'])) {
            $this->__name = $request->parameters['name'];

        }
        if (isset($request->parameters['age'])) {
            $this->__age = $request->parameters['age'];
        }
        if (isset($request->parameters['phone'])) {
            $this->__phone = $request->parameters['phone'];
        }
        return true;
    }

    /**
     * @param $id
     */
    public function setId ($id)
    {
        $this->__id = $id;
    }

    /**
     * @return mixed
     */
    public function getId ()
    {
        return $this->__id;
    }

    /**
     * @param $name
     */
    public function setName ($name)
    {
        $this->__name = $name;
    }

    /**
     * @return mixed
     */
    public function getName ()
    {
        return $this->__name;
    }

    /**
     * @param $age
     */
    public function setAge ($age)
    {
        $this->__age = $age;
    }

    /**
     * @return mixed
     */
    public function getAge ()
    {
        return $this->__age;
    }

    /**
     * @param $phone
     */
    public function setPhone ($phone)
    {
        $this->__phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPhone ()
    {
        return $this->__phone;
    }

    /**
     * @return array
     */
    public function toArray ()
    {
        $staff_array = array();
        if (!empty($this->__id)) {
            $staff_array['id'] = $this->__id;
        }
        return array_merge($staff_array, $this->getProperty());
    }

    /**
     *
     */
    public function getProperty ()
    {
        $staff_array = array();
        if (!empty($this->__name)) {
            $staff_array['name'] = $this->__name;
        }
        if (!empty($this->__age)) {
            $staff_array['age'] = $this->__age;
        }
        if (!empty($this->__phone)) {
            $staff_array['phone'] = $this->__phone;
        }
        return $staff_array;
    }

    function is_search ()
    {
        return $this->__is_search;
    }

    public function get_search_condition ()
    {
        return $this->__search_condition;
    }

    public function set_search_condition ($request)
    {
        if (!empty($request->url_elements[2])) {
            $this->__search_condition = $request->url_elements[2];
        }
    }
}