<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 22/06/2017
 * Time: 23:10
 */
class BussinessModel extends MyModel
{
    protected $_key = 'id';

    function __construct ()
    {
        parent::connect();
    }

    function __destruct ()
    {
        parent::dis_connect();
    }

    function add_new ($staff)
    {
        $id = parent::insert($staff->getProperty());
        $staff->setId($id);
        return $staff;
    }

    function delete_by_id ($id)
    {
        if ($this->remove($this->_key . '=' . $id) == 0) {
            return false;
        }
        return array('id' => $id);
    }

    function update_by_id ($staff)
    {
        $resual = $this->update($staff->getProperty(), $this->_key . "=" . (int)$staff->getId());
        if ($resual == false) {
            echo 'Err while update staff!';
            exit(1);
        }
        return $staff;
    }

    function select_by_id ($select, $id)
    {
        if (empty($id)) {
            $sql = 'SELECT ' . $select . ' FROM staff';
            return $this->get_list($sql);
        } else {
            $sql = "select $select from staff where isdeleted = 0 AND " . $this->_key . " = " . (int)$id;
            return $this->get_row($sql);
        }
    }

    function search_in_table ($table_name, $condition)
    {
        $sql = "SELECT id,name,age,phone FROM " . $table_name . " WHERE " .
            implode(" OR ", $this->get_querry_like($table_name, $condition));
        return $this->get_list($sql);
    }
}