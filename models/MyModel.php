<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 22/06/2017
 * Time: 21:52
 */
class MyModel
{
    private $__conn;

    function connect ()
    {
        if (!$this->__conn) {
            $this->__conn = mysqli_connect('mysql', 'dev', 'dev', 'test')
            or die ('Lỗi kết nối');
        }
    }

    function dis_connect ()
    {
        if ($this->__conn) {
            mysqli_close($this->__conn);
        }
    }

    function insert ($data)
    {
        $this->connect();
        $field_list = '';
        $value_list = '';
        foreach ($data as $key => $value) {
            $field_list .= ",$key";
            $value_list .= ",'" . $value . "'";
        }
        $sql = 'INSERT INTO staff (' . trim($field_list, ',') . ') VALUES (' . trim($value_list, ',') . ')';
        if (!mysqli_query($this->__conn, $sql)) {
            return false;
        }
        return mysqli_insert_id($this->__conn);
    }

    function update ($data, $where)
    {
        $this->connect();
        $sql = '';
        foreach ($data as $key => $value) {
            $sql .= "$key = '" . $value . "',";
        }
        $sql = 'UPDATE staff SET ' . trim($sql, ',') . ' WHERE ' . $where;
        return mysqli_query($this->__conn, $sql);
    }

    function remove ($where)
    {
        $this->connect();
        $sql = "DELETE FROM staff WHERE $where";
        return mysqli_query($this->__conn, $sql);
    }

    function get_list ($sql)
    {
        $this->connect();
        $result = mysqli_query($this->__conn, $sql);
        if (!$result) {
            die ('Câu truy vấn bị sai');
        }
        $return = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $return[] = $row;
        }
        mysqli_free_result($result);
        return $return;
    }

    function get_row ($sql)
    {
        $this->connect();
        $result = mysqli_query($this->__conn, $sql);
        if (!$result) {
            die ('Câu truy vấn bị sai');
        }
        $row = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        if (!$row) {
            return false;
        }
        return $row;
    }

    function get_column_name ($table_name)
    {
        $this->connect();
        $sql = "SHOW COLUMNS FROM ".$table_name;
        return mysqli_query($this->__conn, $sql);
    }

    function get_querry_like ($table_name, $condition)
    {
        $querry_like = array();
        $column = $this->get_column_name($table_name);
        while ($columns = mysqli_fetch_array($column)) {
            $querry_like[] = $columns[0] . " LIKE '%" . $condition . "%'";
        }
        return $querry_like;
    }
}