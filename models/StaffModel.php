<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 18/06/2017
 * Time: 23:54
 */
class StaffModel extends BussinessModel
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $staff
     * @return array
     */
    public function get($staff)
    {
        return parent::select_by_id('id,name,age,phone',$staff->getId());
    }

    public function search ($staff)
    {
        return parent::search_in_table('staff',$staff->get_search_condition());
    }

    /**
     * @param $staff
     * @return array|bool
     */
    public function add($staff){
        return parent::add_new($staff);
    }

    /**
     * @param $id
     * @return array|bool
     */
    public function delete($staff){
        return parent::delete_by_id($staff->getId());
    }

    /**
     * @param $staff
     * @return bool|mysqli_result
     */
    public function edit($staff)
    {
        return parent::update_by_id($staff);
    }
}