<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 22/06/2017
 * Time: 21:35
 */
class MyController
{

    /**
     * @param $request
     * @param $staff
     */
    function callView($request, $staff)
    {
        $view_name = ucfirst($request->format) . 'View';
        if (class_exists($view_name)) {
            $view = new $view_name();
            $view->render($staff);
        }
    }

    /**
     * @param $request
     * @return bool|StaffObject
     */
    function get_obj($request,$obj){
        $obj_name = $obj.'Object';
        if(!class_exists($obj_name)){
            return false;
        }
        return new $obj_name($request);
    }
}