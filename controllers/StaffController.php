<?php

/**
 * Created by PhpStorm.
 * User: totoro
 * Date: 18/06/2017
 * Time: 23:12
 */
Class StaffController extends MyController
{
    /**
     * @param $request
     * @return bool
     */
    public function getAction ($request)
    {
        if($this->get_obj($request,'Staff')->is_search()){
            $staff_obj = $this->get_staff_model()->search($this->get_obj($request,'Staff'));
            parent::callView($request,$staff_obj);
            return true;
        }
        $staff_obj = $this->get_staff_model()->get($this->get_obj($request,'Staff'));
        parent::callView($request,$staff_obj);
    }

    /**
     * @param $request
     */
    public function postAction ($request)
    {
        $staff_obj = $this->get_staff_model()->add(parent::get_obj($request,'Staff'));
        parent::callView($request, $staff_obj->toArray());
    }

    /**
     * @param $request
     */
    public function deleteAction ($request)
    {
        $staff_id = $this->get_staff_model()->delete(parent::get_obj($request,'Staff'));
        parent::callView($request, $staff_id);
    }

    /**
     * @param $request
     */
    public function putAction ($request)
    {
        $staff_obj = $this->get_staff_model()->edit(parent::get_obj($request,'Staff'));
        parent::callView($request, $staff_obj->toArray());
    }

    function get_staff_model(){
        return new StaffModel();
    }
}