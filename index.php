<?php
spl_autoload_register('apiAutoload');
function apiAutoload ($class_name)
{
    if (preg_match('/[a-zA-Z]+Controller$/', $class_name)) {
        include __DIR__ . '/controllers/' . $class_name . '.php';
        return true;
    } elseif (preg_match('/[a-zA-Z]+Model$/', $class_name)) {
        include __DIR__ . '/models/' . $class_name . '.php';
        return true;
    } elseif (preg_match('/[a-zA-Z]+View$/', $class_name)) {
        include __DIR__ . '/views/' . $class_name . '.php';
        return true;
    } elseif (preg_match('/[a-zA-Z]+Object$/', $class_name)) {
        include __DIR__ . '/objects/' . $class_name . '.php';
        return true;
    }
    return false;
}

$request = new Request();
$request->routing();

class Request
{
    public $url_elements;
    public $method;
    public $parameters;

    /**
     * Request constructor.
     */
    public function __construct ()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->url_elements = explode('/', $_GET['controller']);
        if (!$this->parseIncomingParams()) {
            echo 'Get content false!';
            return false;
        }
        $this->format = 'json';
        if (isset($this->parameters['format'])) {
            $this->format = $this->parameters['format'];
        }
        return true;
    }

    /**
     * @return bool
     */
    public function parseIncomingParams ()
    {
        if (isset($_SERVER['QUERY_STRING'])) {
            parse_str($_SERVER['QUERY_STRING'], $parameters);
        }
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        return $this->getParameter($content_type, $body);

    }

    /**
     * @return bool
     */
    public function routing ()
    {
        $controller_name = ucfirst($this->url_elements[0]) . 'Controller';
        if (!class_exists($controller_name)) {
            echo 'No Resource Found!';
            return false;
        }
        $controller = new $controller_name();
        $action_name = strtolower($this->method) . 'Action';
        if (!method_exists($controller_name, $action_name)) {
            echo 'No Action Found!';
            return false;
        }
        $controller->$action_name($this);
        return true;
    }

    /**
     * @param $content_type
     * @param $body
     * @return bool
     */
    public function getParameter ($content_type, $body)
    {
        $parameters = array();
        if (!$content_type) {
            return false;
        }
        if ($content_type == 'application/json') {
            $body_params = json_decode($body);
            if ($body_params) {
                foreach ($body_params as $param_name => $param_value) {
                    $parameters[$param_name] = $param_value;
                }
            }
            $this->format = "json";
        } elseif ($content_type == 'application/x-www-form-urlencoded') {
            parse_str($body, $postvars);
            foreach ($postvars as $field => $value) {
                $parameters[$field] = $value;
            }
            $this->format = "html";
        } else {
            return false;
        }
        $this->parameters = $parameters;
        return true;
    }
}
